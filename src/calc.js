// src/calc.js

/**
* Adds two numbers together.
* @param {number} a 
* @param {number} b 
* @returns {number}
*/
const add = (a, b) => a + b;

/**
 * Subtracts a number from other number.
 * @param {number} minuend
 * @param {number} subtrahend
 * @returns {number}
 */
const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

/**
 * Performs an arithmetic division operation.
 * @param {number} dividend 
 * @param {number} divisor
 * @returns {number}
 * @throws {Error} 0 division
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 division is not allowed");
    const fraction = dividend / divisor;
    return fraction;
};

/**
 * Performs an arithmetic multiplication operation.
 * @param {number} multiplier 
 * @param {number} multiplicant 
 * @returns {number}
 */
const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
};

export default { add, subtract, divide, multiply }