# Calculator library testing project

This is a very simple example project which includes a NodeJS app, a math library and some unit tests.

1. Calculator library (src/calc.js) includes functions for the 4 basic arithmetic operations (addition, division, subtraction and multiplication).
2. Unit testing with Mocha and Chai.
3. NodeJS app (src/main.js) creates local endpoints which can be accessed using a web browser.

Usage:
* 'npm start' to start the app.
* 'npm run dev' to start the app in development mode (watching source code changes).
* 'npm test' to run the unit tests.