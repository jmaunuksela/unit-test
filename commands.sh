#!/bin/bash
mkdir -p /stam/L03/unit-test
# code ~/projects/stam/L03/unit-test
git init
npm init -y
npm -i
npm install --save express
npm install --save-dev mocha chai
echo "node_modules" > .gitignore
# git remote add origin https://gitlab.com/jmaunuksela/unit-test
# git push -u origin main 
git remote -v

# test files that goes to staging area
# git add . --dry-run

git push --set-upstream origin master

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js
